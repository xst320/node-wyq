var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/index', function(req, res, next) {
  res.render('index', { title: '主首页' });
});

// 网站地图
router.get('/sitemap', function(req, res, next) {
    res.render('sitemap', { title: '网站地图' });
});

// 警告层
router.get('/alert', function(req, res, next) {
    res.render('alert', { title: '警告层' });
});


// 创作者
router.get('/author/author_person', function(req, res, next) {
    res.render('author/author_person', { title: '创作者-找个人' });
});
router.get('/author/author_org', function(req, res, next) {
    res.render('author/author_org', { title: '创作者-找机构' });
});
router.get('/author/author_attention', function(req, res, next) {
    res.render('author/author_attention', { title: '创作者-我的创作者-我关注的创作者' });
});
router.get('/home/home', function(req, res, next) {
    res.render('home/home', { title: '创作者-个人页-首页' });
});
router.get('/home/wor_video', function(req, res, next) {
    res.render('home/wor_video', { title: '创作者-个人页-作品-视频作品' });
});
router.get('/home/wor_scr', function(req, res, next) {
    res.render('home/wor_scr', { title: '创作者-个人页-作品-剧本作品' });
});
router.get('/home/art_article', function(req, res, next) {
    res.render('home/art_article', { title: '创作者-个人页-文章-文章' });
});
router.get('/home/art_column', function(req, res, next) {
    res.render('home/art_column', { title: '创作者-个人页-文章-专栏' });
});
router.get('/home/pho_album', function(req, res, next) {
    res.render('home/pho_album', { title: '创作者-个人页-照片-相册' });
});
router.get('/home/pho_album_show', function(req, res, next) {
    res.render('home/pho_album_show', { title: '创作者-个人页-照片-相册-相册内页' });
});
router.get('/home/pho_album_edit', function(req, res, next) {
    res.render('home/pho_album_edit', { title: '创作者-个人页-照片-相册-编辑相册' });
});
router.get('/home/pho_photo', function(req, res, next) {
    res.render('home/pho_photo', { title: '创作者-个人页-照片-照片' });
});
router.get('/home/pho_views', function(req, res, next) {
    res.render('home/pho_views', { title: '创作者-个人页-照片-照片-查看照片' });
});
router.get('/home/pho-edit', function(req, res, next) {
    res.render('home/pho-edit', { title: '创作者-个人页-照片-照片-编辑照片' });
});
router.get('/home/pho_upload', function(req, res, next) {
    res.render('home/pho_upload', { title: '创作者-个人页-照片-上传照片' });
});
router.get('/home/gro_topic', function(req, res, next) {
    res.render('home/gro_topic', { title: '创作者-个人页-小组-话题' });
});
router.get('/home/gro_group', function(req, res, next) {
    res.render('home/gro_group', { title: '创作者-个人页-小组-小组' });
});
router.get('/home/squ_info', function(req, res, next) {
    res.render('home/squ_info', { title: '创作者-个人页-广场-信息' });
});
router.get('/home/fri_attention', function(req, res, next) {
    res.render('home/fri_attention', { title: '创作者-个人页-好友-关注' });
});
router.get('/home/fri_fans', function(req, res, next) {
    res.render('home/fri_fans', { title: '创作者-个人页-好友-粉丝' });
});


// 作品
router.get('/works/works_video_list', function(req, res, next) {
    res.render('works/works_video_list', { title: '作品-视频作品' });
});
router.get('/works/works_video_content', function(req, res, next) {
    res.render('works/works_video_content', { title: '作品-视频作品内页' });
});
router.get('/works/works_video_pub', function(req, res, next) {
    res.render('works/works_video_pub', { title: '作品-发布视频作品' });
});
router.get('/works/works_screenplay_list', function(req, res, next) {
    res.render('works/works_screenplay_list', { title: '作品-剧本作品' });
});
router.get('/works/works_screenplay_content', function(req, res, next) {
    res.render('works/works_screenplay_content', { title: '作品-剧本作品内页' });
});
router.get('/works/works_screenplay_pub', function(req, res, next) {
    res.render('works/works_screenplay_pub', { title: '作品-发布剧本作品' });
});
router.get('/works/works_video_col', function(req, res, next) {
    res.render('works/works_video_col', { title: '作品-我收藏的视频作品' });
});
router.get('/works/works_screenplay_col', function(req, res, next) {
    res.render('works/works_screenplay_col', { title: '作品-我收藏的视频作品' });
});


// 专栏
router.get('/column/column_myatt', function(req, res, next) {
    res.render('column/column_myatt', { title: '专栏-我的关注' });
});
router.get('/column/column_unmyatt', function(req, res, next) {
    res.render('column/column_unmyatt', { title: '专栏-我的关注-未关注专栏' });
});
router.get('/column/column_article_list', function(req, res, next) {
    res.render('column/column_article_list', { title: '专栏-最新文章' });
});
router.get('/column/column_article_content', function(req, res, next) {
    res.render('column/column_article_content', { title: '专栏-文章内页' });
});
router.get('/column/column_article_hots', function(req, res, next) {
    res.render('column/column_article_hots', { title: '专栏-热门文章' });
});
router.get('/column/column_rec', function(req, res, next) {
    res.render('column/column_rec', { title: '专栏-专栏推荐' });
});
router.get('/column/column_content', function(req, res, next) {
    res.render('column/column_content', { title: '专栏-专栏内页' });
});
router.get('/column/column_article_pub', function(req, res, next) {
    res.render('column/column_article_pub', { title: '专栏-发表文章' });
});
router.get('/column/column_edit', function(req, res, next) {
    res.render('column/column_edit', { title: '专栏-编辑专栏' });
});
router.get('/column/column_myarticle', function(req, res, next) {
    res.render('column/column_myarticle', { title: '专栏-我的专栏-我发表的文章' });
});
router.get('/column/column_myatt_col', function(req, res, next) {
    res.render('column/column_myatt_col', { title: '专栏-我的专栏-我关注的专栏' });
});


// 小组
router.get('/group/group_mytopic', function(req, res, next) {
    res.render('group/group_mytopic', { title: '小组-我的话题' });
});
router.get('/group/group_mytopic_unjoin', function(req, res, next) {
    res.render('group/group_mytopic_unjoin', { title: '小组-我的话题-未加入小组' });
});
router.get('/group/group_topic_list', function(req, res, next) {
    res.render('group/group_topic_list', { title: '小组-最新话题' });
});
router.get('/group/group_topic_hots', function(req, res, next) {
    res.render('group/group_topic_hots', { title: '小组-热门话题' });
});
router.get('/group/group_rec', function(req, res, next) {
    res.render('group/group_rec', { title: '小组-小组推荐' });
});
router.get('/group/group_topic_content', function(req, res, next) {
    res.render('group/group_topic_content', { title: '小组-话题内页' });
});
router.get('/group/group_content', function(req, res, next) {
    res.render('group/group_content', { title: '小组-小组内页' });
});
router.get('/group/group_topic_pub', function(req, res, next) {
    res.render('group/group_topic_pub', { title: '小组-发新话题' });
});
router.get('/group/group_create', function(req, res, next) {
    res.render('group/group_create', { title: '小组-创建小组' });
});
router.get('/group/group_topic_par', function(req, res, next) {
    res.render('group/group_topic_par', { title: '小组-我的小组-我参与的话题' });
});
router.get('/group/group_join', function(req, res, next) {
    res.render('group/group_join', { title: '小组-我的小组-我加入的小组' });
});


// 集市
router.get('/market/market_list', function(req, res, next) {
    res.render('market/market_list', { title: '集市-最新发布' });
});
router.get('/market/market_talent', function(req, res, next) {
    res.render('market/market_talent', { title: '集市-人员招募' });
});
router.get('/market/market_info_pub', function(req, res, next) {
    res.render('market/market_info_pub', { title: '集市-发布信息' });
});
router.get('/market/market_info_content', function(req, res, next) {
    res.render('market/market_info_content', { title: '集市-信息内页' });
});
router.get('/market/market_my', function(req, res, next) {
    res.render('market/market_my', { title: '集市-我的集市' });
});


// 搜索
router.get('/search/search_all', function(req, res, next) {
    res.render('search/search_all', { title: '搜索-全部' });
});
router.get('/search/search_person', function(req, res, next) {
    res.render('search/search_person', { title: '搜索-个人' });
});
router.get('/search/search_org', function(req, res, next) {
    res.render('search/search_org', { title: '搜索-机构' });
});
router.get('/search/search_video', function(req, res, next) {
    res.render('search/search_video', { title: '搜索-视频' });
});
router.get('/search/search_screenplay', function(req, res, next) {
    res.render('search/search_screenplay', { title: '搜索-剧本' });
});
router.get('/search/search_article', function(req, res, next) {
    res.render('search/search_article', { title: '搜索-文章' });
});
router.get('/search/search_column', function(req, res, next) {
    res.render('search/search_column', { title: '搜索-专栏' });
});
router.get('/search/search_topic', function(req, res, next) {
    res.render('search/search_topic', { title: '搜索-话题' });
});
router.get('/search/search_group', function(req, res, next) {
    res.render('search/search_group', { title: '搜索-小组' });
});
router.get('/search/search_info', function(req, res, next) {
    res.render('search/search_info', { title: '搜索-信息' });
});

// 用户
router.get('/member/login', function(req, res, next) {
    res.render('member/login', { title: '登录' });
});
router.get('/member/register', function(req, res, next) {
    res.render('member/register', { title: '注册' });
});
router.get('/member/for_password', function(req, res, next) {
    res.render('member/for_password', { title: '忘记密码' });
});
router.get('/member/mod_password', function(req, res, next) {
    res.render('member/mod_password', { title: '修改密码' });
});
router.get('/member/mail_failure', function(req, res, next) {
    res.render('member/mail_failure', { title: '邮件链接已过期' });
});
router.get('/member/mail_success', function(req, res, next) {
    res.render('member/mail_success', { title: '邮件验证成功' });
});
router.get('/member/mes_per', function(req, res, next) {
    res.render('member/mes_per', { title: '用户-消息中心-用户私信' });
});
router.get('/member/mes_sys', function(req, res, next) {
    res.render('member/mes_sys', { title: '用户-消息中心-系统消息' });
});
router.get('/member/mes_per_show', function(req, res, next) {
    res.render('member/mes_per_show', { title: '用户-消息中心-私信内页' });
});
router.get('/member/acc_per_data', function(req, res, next) {
    res.render('member/acc_per_data', { title: '用户-账户设置-个人资料' });
});
router.get('/member/acc_mod_email', function(req, res, next) {
    res.render('member/acc_mod_email', { title: '用户-账户设置-修改邮箱' });
});
router.get('/member/acc_mod_password', function(req, res, next) {
    res.render('member/acc_mod_password', { title: '用户-账户设置-修改密码' });
});
router.get('/member/acc_bound', function(req, res, next) {
    res.render('member/acc_bound', { title: '用户-账户设置-绑定账号' });
});
router.get('/member/aut', function(req, res, next) {
    res.render('member/aut', { title: '用户-用户认证' });
});
router.get('/member/aut-person', function(req, res, next) {
    res.render('member/aut-person', { title: '用户-用户认证-个人认证' });
});
router.get('/member/aut_org', function(req, res, next) {
    res.render('member/aut_org', { title: '用户-用户认证-机构认证' });
});



module.exports = router;
