
// TAB
$(function(){
    $('.scr-tab-menu li').mouseover(function(){
        var liindex = $('.scr-tab-menu li').index(this);
        $(this).addClass('cur').siblings().removeClass('cur');
        $('div.tab-con').eq(liindex).fadeIn(150).siblings('div.tab-con').hide();
        var liWidth = $('.scr-tab-menu li').width();
        $('.scr-tab-menu p').stop(false,true).animate({'left' : liindex * liWidth + 'px'},300);
    });
});

// TAB
$(function(){
    $('.tab-menu li').click(function(){
        var liindex = $('.tab-menu li').index(this);
        $(this).addClass('cur').siblings().removeClass('cur');
        $('div.tab-con').eq(liindex).fadeIn(150).siblings('div.tab-con').hide();
        var liWidth = $('.tab-menu li').width();
        $('.tab-menu p').stop(false,true).animate({'left' : liindex * liWidth + 'px'},300);
    });
});


// 分享下拉菜单
$(document).ready(function(){
    $(".share-wrap").hover(function(){
        $(this).addClass("share-show");
    },function(){
        $(this).removeClass("share-show");
    })

})

// 视频作品详情指向显示
$(document).ready(function(){

    $(".works-box").hover(function(){
        $(this).addClass("show");
    },function(){
        $(this).removeClass("show");
    })

})

// 返回顶部
$(function(){
    $(window).scroll(function() {
        if($(window).scrollTop() >= 100){
            $('.gotop').fadeIn(300);
        }else{
            $('.gotop').fadeOut(300);
        }
    });
    $('.gotop').click(function(){
        $('html,body').animate({scrollTop: '0px'}, 400);});
});
