// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
// ����
$(function(){
    // 编辑相册
    $(".js_edit_album").modal({
        trigger : ".js_edit_album",
        modals : "#js_edit_album",
        animationEffect : "fadeIn",
        popPosition : {						  //弹窗的最后位置
            top : "Content",				  //top    "Up" | "Content" | "windowDown" | "bodyDown" | 输入其他默认为 "Content"
            left : "Content"			      //left    "Left" | "Content" | "Right" | 输入其他默认为 "Content"
        },
        olayBe : true,
        close : ".js_close"
    });
    // 编辑照片
    $(".js_edit_pho").modal({
        trigger : ".js_edit_pho",
        modals : "#js_edit_pho",
        animationEffect : "fadeIn",
        popPosition : {
            top : "Content",
            left : "Content"
        },
        olayBe : true,
        close : ".js_close"
    });
    // 会员登录
    $(".js_mem_login").modal({
        trigger : ".js_mem_login",
        modals : "#js_mem_login",
        animationEffect : "fadeIn",
        popPosition : {
            top : "Content",
            left : "Content"
        },
        olayBe : true,
        close : ".js_close"
    });
    // 分享到金象动态
    $(".js_mem_share").modal({
        trigger : ".js_mem_share",
        modals : "#js_mem_share",
        animationEffect : "fadeIn",
        popPosition : {
            top : "Content",
            left : "Content"
        },
        olayBe : true,
        close : ".js_close"
    });
    // 发私信
    $(".js_private_msg").modal({
        trigger : ".js_private_msg",
        modals : "#js_private_msg",
        animationEffect : "fadeIn",
        popPosition : {
            top : "Content",
            left : "Content"
        },
        olayBe : true,
        close : ".js_close"
    });
    // 提示框
    $(".js_prompt").modal({
        trigger : ".js_prompt",
        modals : "#js_prompt",
        animationEffect : "fadeIn",
        popPosition : {
            top : "Content",
            left : "Content"
        },
        olayBe : true,
        close : ".js_close"
    });
    // 个人详情
    $(".js_intro").modal({
        trigger : ".js_intro",
        modals : "#js_intro",
        animationEffect : "fadeIn",
        popPosition : {
            top : "Content",
            left : "Content"
        },
        olayBe : true,
        close : ".js_close"
    });
});

